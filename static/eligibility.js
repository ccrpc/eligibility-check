document.addEventListener('DOMContentLoaded', function() {
    // Get query parameters from the URL
    const urlParams = new URLSearchParams(window.location.search);
    
    // Get the income, household size, and other parameters from the query parameters
    const income = parseInt(urlParams.get('income'));
    const householdSize = parseInt(urlParams.get('householdSize'));
    const isHomeless = urlParams.get('isHomeless');
    const isOver60 = urlParams.get('isOver60');
    const isChampaignCounty = urlParams.get('isChampaignCounty');

    // Display the results based on the query parameters
    displayLIHEAPResults(income, householdSize);
    displaySeniorServicesResults(isOver60, isChampaignCounty);
    displayHousingServicesResults(isHomeless);
    displayWeatherizationResults(income, householdSize);
});

// Function to display LIHEAP eligibility results
function displayLIHEAPResults(income, householdSize) {
    const liheapEligibilityResult = document.getElementById('liheap-eligibility-result');
    const annualIncome = income * 12;

    // Check LIHEAP eligibility based on income and household size
    if (_computeIsEligibleForLIHEAPandWeatherization(householdSize, income)) {
        liheapEligibilityResult.textContent = `You might be eligible for LIHEAP`;
        liheapEligibilityResult.className = 'eligible';
    } else {
        liheapEligibilityResult.textContent = `You might not be eligible for LIHEAP`;
        liheapEligibilityResult.className = 'not-eligible';
    }
}

// Function to display senior services eligibility results
function displaySeniorServicesResults(isOver60, isChampaignCounty) {
    const seniorServicesEligibilityResult = document.getElementById('senior-services-eligibility-result');

    // Check senior services eligibility based on age and location
    if (isOver60 === 'yes' && isChampaignCounty === 'yes') {
        seniorServicesEligibilityResult.textContent = `You might be eligible for Senior Services`;
        seniorServicesEligibilityResult.className = 'eligible';
    } else {
        seniorServicesEligibilityResult.textContent = `You might not be eligible for Senior Services`;
        seniorServicesEligibilityResult.className = 'not-eligible';
    }
}

// Function to display housing services eligibility results
function displayHousingServicesResults(isHomeless) {
    const housingServicesResult = document.getElementById('housing-services-eligibility-result');

    // Check housing services eligibility based on "homeless" status
    if (isHomeless === 'yes') {
        housingServicesResult.textContent = `You might be eligible for Housing Services`;
        housingServicesResult.className = 'eligible';
    } else {
        housingServicesResult.textContent = `You might not be eligible for Housing Services`;
        housingServicesResult.className = 'not-eligible';
    }
}

// Function to display weatherization services eligibility results
function displayWeatherizationResults(income, householdSize, isHomeless) {
    const weatherizationEligibilityResult = document.getElementById('weatherization-eligibility-result');
    const annualIncome = income * 12;

    // Check weatherization services eligibility based on income and household size
    if (_computeIsEligibleForLIHEAPandWeatherization(householdSize, income) && isHomeless === 'no') {
        weatherizationEligibilityResult.textContent = `You might be eligible for Weatherization Services`;
        weatherizationEligibilityResult.className = 'eligible';
    } else {
        weatherizationEligibilityResult.textContent = `You might not be eligible for Weatherization Services`;
        weatherizationEligibilityResult.className = 'not-eligible';
    }
}

// Function to compute LIHEAP eligibility
function _computeIsEligibleForLIHEAPandWeatherization(householdSize, income) {
    const FED_GUIDELINE = {
        1: 1215.00,
        2: 1643.33,
        3: 2071.67,
        4: 2500.00,
        5: 2928.33,
        6: 3356.67,
        7: 3785.00,
        8: 4213.33
    };
    const LIHEAP_MULTIPLIER = 2;

    // Calculate income guideline
    let mulGuide = {};
    if (householdSize >= 9) {
        mulGuide[householdSize] = (FED_GUIDELINE[8] + ((householdSize - 8) * 428.33)) * LIHEAP_MULTIPLIER;
    }
    for (let key in FED_GUIDELINE) {
        mulGuide[key] = FED_GUIDELINE[key] * LIHEAP_MULTIPLIER;
    }

    // Check income eligibility
    return income <= mulGuide[householdSize];
}
