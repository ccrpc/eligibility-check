document.addEventListener('DOMContentLoaded', function() {
    var eligibilityForm = document.getElementById('eligibility-form');
    var incomeInput = document.getElementById('income');
    var householdSizeInput = document.getElementById('people');
    var isChampaignCounty= document.getElementById('residence');
    var isHomeless = document.getElementById('homeless');
    var isOver60 = document.getElementById('over-60');
    var customDialog = document.getElementById('custom-dialog');
    var confirmYesButton = document.getElementById('confirm-yes');
    var confirmNoButton = document.getElementById('confirm-no');

    // Attach event listener to the form submit button
    eligibilityForm.addEventListener('submit', function(event) {
        event.preventDefault();
        var income = parseInt(incomeInput.value);
        var householdSize = parseInt(householdSizeInput.value);
        
        // Show custom dialog
        var dialogMessage = 'You have stated your monthly income as $' + income + '. This makes your annual income as $' + (income * 12) + '. Is this correct?';
        document.getElementById('dialog-message').textContent = dialogMessage;
        customDialog.style.display = 'block';
    });

    // Confirm yes button action
    confirmYesButton.addEventListener('click', function() {
        // Redirect to result page with query parameters
        window.location.href = '/eligibility-check/results/?income=' + incomeInput.value + '&householdSize=' + householdSizeInput.value + '&isHomeless=' + isHomeless.value + '&isOver60=' + isOver60.value + '&isChampaignCounty=' + isChampaignCounty.value; 
    });

    // Confirm no button action
    confirmNoButton.addEventListener('click', function() {
        customDialog.style.display = 'none';
        window.location.href = '/eligibility-check#heading-form';
    });
});
