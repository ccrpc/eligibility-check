---
title: Home
draft: false
weight: 10
bannerHeading: Check Eligibility
bannerText: This is a demo website build to represent what the tool can be like
bannerUrl: banner.jpg
---

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LIHEAP Eligibility Form</title>
</head>
<body>
    <div id="form-container">
        <div id="form-header">
            <h1>Eligibility Checker Tool</h1>
        </div>
        <div id="form-content">
            <p>This is a tool developed by Champaign County Regional Planning Commission to give you an idea of whether you might be eligible for its programs.</p>
            <div id="add-instructions">
                <h4><u>This tool is not an official application to the program, nor does it guarantee that you will be accepted into the program. It is merely a tentative opinion of your eligibility based on income guidelines. You still need to apply via the official channel and the case managers at CCRPC will determine the final approval into the program.</u></h4>
            </div>
            <hr />
            <h2 class="red-underline-header" id="heading-form">Please answer the following questions</h2>
            <br />
            <form id="eligibility-form" method="GET">
                <label class="red-underline-header-smaller" for="residence">1. Do you reside within the Champaign County?</label>
                <select id="residence" name="residence" required>
                    <option value="">Select an option</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label class="red-underline-header-smaller" for="aian-status">2. LIHEAP eligibility requirements can also vary for members of an American Indian or Alaska Native (AIAN) tribe. Are you a member of an AIAN tribe?</label>
                <select id="aian-status" name="aian-status" required>
                    <option value="">Select an option</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label class="red-underline-header-smaller" for="people">3. How many people live in your home (including yourself)?
                    <span class="tooltip">
                        <span class="tooltip-icon"><i class="fa fa-question-circle"></i></span>
                        <span class="tooltiptext">
                            Household members include all people living in your home, such as relatives, partners, roommates, etc., that are covered by the same utility bill.
                        </span>
                    </span>        
                </label>
                <select id="people" name="people" required>
                    <option value="">Select an option</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12more">12 or more</option>
                </select>
                <label class="red-underline-header-smaller" for="income">4. What is your household's monthly income (in dollars) before taxes?
                    <span class="tooltip">
                        <span class="tooltip-icon"><i class="fas fa-question-circle"></i></span>
                        <span class="tooltiptext">
                            Income includes any income received by any household member. Include all forms of income (such as wages, unemployment, SSI, child support, etc.) but DO NOT INCLUDE benefits earned from SNAP or WIC.
                            <br><br>
                            DISCLAIMER: Which types of income count varies by state and tribe. To find out which types of income count in your state or tribe, contact your local LIHEAP office or tribal LIHEAP office.
                        </span>
                    </span>
                </label>
                <div class="income-input">
                    <span class="dollar-sign">$</span>
                    <input type="number" id="income" name="income" required>
                </div>
                <div class="annual-income-input">
                    <label for="annual-income">Annual Income:</label>
                    <span class="dollar-sign">$</span>
                    <input type="text" id="annual-income" name="annual-income" readonly>
                </div>
                <div id="custom-dialog" class="dialog">
                    <div class="dialog-content">
                        <h3>Confirm Income</h3>
                        <p id="dialog-message"></p>
                        <div class="dialog-buttons">
                            <button id="confirm-yes">Yes</button>
                            <button id="confirm-no">No</button>
                        </div>
                    </div>
                </div>
                <label class="red-underline-header-smaller" for="benefits">5. Are you or any other members of your household currently receiving benefits from any of the following programs? (Check all that apply)</label>
                <div style="visibility:hidden; color:red;" id="chk_option_error">Please select at least one option.</div>
                <div class="checkbox-group">
                    <div>
                        <input type="checkbox" id="snap" name="benefits[]" value="SNAP">
                        <label for="snap">Supplemental Nutrition Assistance Program (SNAP)</label>
                    </div>
                    <div>
                        <input type="checkbox" id="ssi" name="benefits[]" value="SSI">
                        <label for="ssi">Supplemental Security Income (SSI)</label>
                    </div>
                    <div>
                        <input type="checkbox" id="tanf" name="benefits[]" value="TANF">
                        <label for="tanf">Temporary Assistance for Needy Families (TANF)</label>
                    </div>
                    <div>
                        <input type="checkbox" id="veterans" name="benefits[]" value="Veterans">
                        <label for="veterans">Means-tested Veterans Benefits
                            <span class="tooltip">
                                <span class="tooltip-icon"><i class="fas fa-question-circle"></i></span>
                                <span class="tooltiptext">
                                    Means-tested Veterans Benefits are veteran’s benefits based on income such as a VA Pension or Aid and Attendance.
                                </span>
                            </span>
                        </label>
                    </div>
                </div>
                <label class="red-underline-header-smaller" for="homeless">6. Are you currently experiencing homelessness?
                <span class="tooltip">
                    <span class="tooltip-icon"><i class="fas fa-question-circle"></i></span>
                    <span class="tooltiptext">
                        Persons served must meet the Department of Housing and Urban Development’s <a target="_blank" href="https://files.hudexchange.info/resources/documents/HomelessDefinition_RecordkeepingRequirementsandCriteria.pdf">definition of homeless. </a>
                    </span>
                </span>
                </label>
                <select id="homeless" name="homeless" required>
                    <option value="">Select an option</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label class="red-underline-header-smaller" for="over-60">7. Are you over 60 years old?</label>
                <select id="over-60" name="over-60" required>
                    <option value="">Select an option</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <button type="submit" class="submit-button">Submit</button>
            </form>
        </div>
    </div>
    <!-- Font Awesome CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/js/all.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <!-- Font Awesome CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" href="/eligibility-check/styles.css">
    <script src="form.js"></script>
</body>
</html>