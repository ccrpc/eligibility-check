---
title: Eligibility Results
weight: 10
bannerHeading: Check Eligibility
bannerText: This is a demo website build to represent what the tool can be like
bannerUrl: banner.jpg
menu: 
---

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eligibility Results</title>
</head>
<body>
    <div class="container">
        <div class="disclaimer card">
            <div class="red-underline-header"><i class="fa-solid fa-exclamation"></i> Disclaimer</div>
            <p>This tool is not an official application to the program, nor does it guarantee that you will be accepted into the program. It is merely a tentative opinion of your eligibility based on income guidelines. You still need to apply via the official channel and the case managers at CCRPC will determine the final approval into the program.</p>
        </div>
        <div class="results card">
            <h2 id="liheap-eligibility-result" class="result-section"></h2>
            <h2 id="senior-services-eligibility-result" class="result-section"></h2>
            <h2 id="housing-services-eligibility-result" class="result-section"></h2>
            <h2 id="weatherization-eligibility-result" class="result-section"></h2>
        </div>
        <div class="next-steps card">
            <div class="red-underline-header"><i class="fa-solid fa-check"></i> What Do I Do Next?</div>
            <p><strong>To find out if you are eligible to receive any benefits for the above categories and for assistance applying, contact <a><u>Champaign County Regional Planning Commission  </u>.</a></strong></p>
        </div>
		<div class="otherBenefits card">
			<div class="red-underline-header"> You Might Be Eligible For Other Benefits</div>
			<p>We encourage you to take the <a href="https://www.benefits.gov/benefit-finder" target="_blank">Benefits Finder</a> questionnaire on benefits.gov to check your eligibility for over 1,000 benefits.</p>	
		</div>
        <br />
        <button class="reset-button" onclick="goToHomePage()">Restart</button>
    <!-- Font Awesome CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/js/all.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <!-- Font Awesome CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    <link rel="stylesheet" href="/eligibility-check/styles.css">
    <script src="/eligibility-check/eligibility.js"></script>
    <script>
        function goToHomePage() {
            window.location.href = '/eligibility-check';
        }
    </script>    
</body>
</html>